package com.softwaretesting.testing.validator;

import org.springframework.stereotype.Service;

@Service
public class PhoneNumberValidator {

    private static final String PHONE_NUMBER_PATTERN =  "^(\\+49|0)\\d{8,11}";

    public boolean isValid(String phoneNumber) {
        if (phoneNumber == null) {
            return false;
        }
        return phoneNumber.matches(PHONE_NUMBER_PATTERN);
    }
}
