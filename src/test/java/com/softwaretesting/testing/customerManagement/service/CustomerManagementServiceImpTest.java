package com.softwaretesting.testing.customerManagement.service;

import com.softwaretesting.testing.dao.CustomerRepository;
import com.softwaretesting.testing.exception.BadRequestException;
import com.softwaretesting.testing.exception.CustomerNotFoundException;
import com.softwaretesting.testing.model.Customer;
import com.softwaretesting.testing.validator.CustomerValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CustomerManagementServiceImpTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerValidator customerValidator;

    @InjectMocks
    private CustomerManagementServiceImp customerManagementService;

    @Captor
    private ArgumentCaptor<Customer> customerArgumentCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    // Test for listing all customers
    @Test
    void list_ReturnsCustomers() {
        // Arrange
        List<Customer> customers = Arrays.asList(new Customer(Long.parseLong("1001"),"test_user","Test User", "+490001698"), new Customer(Long.parseLong("1002"),"test_user_2","Test User 2", "+490001699"));

        // Mock
        when(customerRepository.findAll()).thenReturn(customers);

        // Act
        Collection<Customer> result = customerManagementService.list();

        // Assert
        assertEquals(customers.size(), result.size());
        verify(customerRepository, times(1)).findAll();
    }

    // Test for finding a customer by username
    @Test
    void findByUserName_ExistingCustomer_ReturnsCustomer() {
        // Arrange
        String userName = "test_user";
        Customer customer = new Customer(Long.parseLong("1001"), userName,"Test User", "+490001698");
        Optional<Customer> optionalCustomer = Optional.of(customer);

        // Stubbing the behavior of the mock objects
        when(customerRepository.findByUserName(userName)).thenReturn(optionalCustomer);
        doNothing().when(customerValidator).validate404(any(), anyString(), anyString());

        // Act
        Customer result = customerManagementService.findByUserName(userName);

        // Assert
        assertEquals(customer, result);
        verify(customerRepository, times(1)).findByUserName(userName);
        verify(customerValidator, times(1)).validate404(any(Optional.class), anyString(), anyString());
    }

    // Test for finding a non-existing customer by username
    @Test
    void findByUserName_NonExistingCustomer_ThrowsCustomerNotFoundException() {
        // Arrange
        String userName = "nonExistingUser";
        Optional<Customer> optionalCustomer = Optional.empty();

        // Mock
        when(customerRepository.findByUserName(userName)).thenReturn(optionalCustomer);
        doThrow(CustomerNotFoundException.class).when(customerValidator).validate404(optionalCustomer, "User-Name", userName);

        // Act & Assert
        assertThrows(CustomerNotFoundException.class, () -> {
            customerManagementService.findByUserName(userName);
        });

        // Verify that the expected interactions occurred
        verify(customerRepository, times(1)).findByUserName(userName);
        verify(customerValidator, times(1)).validate404(optionalCustomer, "User-Name", userName);
    }

    // Test for finding a customer by id
    @Test
    void findById_ExistingCustomer_ReturnsCustomer() {
        // Arrange
        Long id = 1L;
        Customer customer = new Customer(id, "test_user","Test User", "+490001698");
        Optional<Customer> optionalCustomer = Optional.of(customer);

        // Mock
        when(customerRepository.findById(id)).thenReturn(optionalCustomer);
        doNothing().when(customerValidator).validate404(any(), anyString(), anyString());

        // Act
        Customer result = customerManagementService.findById(id);

        // Assert
        assertEquals(customer, result);
        verify(customerRepository, times(1)).findById(id);
        verify(customerValidator, times(1)).validate404(any(), any(), any());
    }

    @Test
    void findById_NonExistingCustomer_ThrowsCustomerNotFoundException() {
        // Arrange
        Long id = 1L;
        Optional<Customer> optionalCustomer = Optional.empty();

        // Mock
        when(customerRepository.findById(id)).thenReturn(optionalCustomer);
        doThrow(CustomerNotFoundException.class).when(customerValidator).validate404(optionalCustomer, "id", String.valueOf(id));

        // Act and Assert
        assertThrows(CustomerNotFoundException.class, () -> {
            customerManagementService.findById(id);
        });

        verify(customerRepository, times(1)).findById(id);
        verify(customerValidator, times(1)).validate404(optionalCustomer, "id", String.valueOf(id));
    }

    @Test
    void selectCustomerByPhoneNumber_ExistingCustomer_ReturnsCustomer() {
        // Arrange
        String phoneNumber = "1234567890";
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Optional<Customer> optionalCustomer = Optional.of(customer);

        // Mock
        when(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).thenReturn(optionalCustomer);
        doNothing().when(customerValidator).validate404(any(), anyString(), anyString());

        // Act
        Customer result = customerManagementService.selectCustomerByPhoneNumber(phoneNumber);

        // Assert
        assertEquals(customer, result);
        verify(customerRepository, times(1)).selectCustomerByPhoneNumber(phoneNumber);
        verify(customerValidator, times(1)).validate404(any(), any(), any());
    }

    @Test
    void selectCustomerByPhoneNumber_NonExistingCustomer_ThrowsCustomerNotFoundException() {
        // Arrange
        String phoneNumber = "nonExistingPhoneNumber";
        Optional<Customer> optionalCustomer = Optional.empty();

        // Mock
        when(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).thenReturn(optionalCustomer);
        doThrow(CustomerNotFoundException.class).when(customerValidator).validate404(optionalCustomer, "phone number", phoneNumber);

        // Act and Assert
        assertThrows(CustomerNotFoundException.class, () -> {
            customerManagementService.selectCustomerByPhoneNumber(phoneNumber);
        });
        verify(customerRepository, times(1)).selectCustomerByPhoneNumber(phoneNumber);
        verify(customerValidator, times(1)).validate404(optionalCustomer, "phone number", phoneNumber);
    }

    @Test
    void delete_ExistingCustomer_DeletesCustomer() {
        // Arrange
        Long customerId = 1L;

        // Mock
        when(customerRepository.existsById(customerId)).thenReturn(true);

        // Act
        customerManagementService.delete(customerId);

        // Assert
        verify(customerRepository, times(1)).existsById(customerId);
        verify(customerRepository, times(1)).deleteById(customerId);
    }

    @Test
    void delete_NonExistingCustomer_ThrowsCustomerNotFoundException() {
        // Arrange
        Long customerId = 1L;

        // Mock
        when(customerRepository.existsById(customerId)).thenReturn(false);

        // Act and Assert
        CustomerNotFoundException ex = assertThrows(CustomerNotFoundException.class, ()->customerManagementService.delete(customerId));
        assertEquals("Customer with id " + customerId + " does not exists", ex.getMessage());

        verify(customerRepository, times(1)).existsById(customerId);
        verify(customerRepository, never()).deleteById(customerId);
    }

    // Test to check if customer with the same phone number raise error
    @Test
    void addCustomer_CustomerWithExistingPhoneNumber_ThrowsBadRequestException() {
        // Arrange
        String phoneNumber = "+490001698";
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Optional<Customer> existingCustomer = Optional.of(new Customer());

        // Mock
        when(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).thenReturn(existingCustomer);

        // Act and Assert
        RuntimeException ex = assertThrows(BadRequestException.class, ()->customerManagementService.addCustomer(customer));
        assertEquals("Phone Number " + customer.getPhoneNumber() + " taken", ex.getMessage());

        verify(customerRepository, times(1)).selectCustomerByPhoneNumber(phoneNumber);
        verify(customerRepository, never()).save(customer);
    }

    // Test to check if customer with unique phone gets saved successfully
    @Test
    void addCustomer_CustomerWithUniquePhoneNumber_SavesCustomer() {
        // Arrange
        String phoneNumber = "+490001698";
        Customer customer = new Customer(Long.parseLong("1001"),"test_user","Test User", phoneNumber);
        Optional<Customer> existingCustomer = Optional.empty();

        // Mock
        when(customerRepository.selectCustomerByPhoneNumber(phoneNumber)).thenReturn(existingCustomer);
        when(customerRepository.save(customer)).thenReturn(customer);

        // Act
        Customer returnedCustomer = customerManagementService.addCustomer(customer);

        // Assert
        // Using captor to check if the save method of CustomerRepository was passed with correct arguments or not
        verify(customerRepository).save(customerArgumentCaptor.capture());
        assertEquals(customer, customerArgumentCaptor.getValue());

        // check if the returned object/value is right
        assertEquals(customer, returnedCustomer);
    }

    // Test for saving all customers
    @Test
    void saveAll_SavesAllCustomers() {
        // Arrange
        List<Customer> customers = Arrays.asList(new Customer(Long.parseLong("1001"),"test_user","Test User", "+490001698"), new Customer(Long.parseLong("1002"),"test_user_2","Test User 2", "+490001699"));

        // Mock
        when(customerRepository.saveAll(customers)).thenReturn(customers);

        // Act
        Collection<Customer> savedCustomers = customerManagementService.saveAll(customers);

        // Assert & Verify
        verify(customerRepository, times(1)).saveAll(customers);
        assertEquals(customers.size(), savedCustomers.size());
        assertTrue(savedCustomers.containsAll(customers)); // Ensure all provided customers are returned as saved

    }
}
