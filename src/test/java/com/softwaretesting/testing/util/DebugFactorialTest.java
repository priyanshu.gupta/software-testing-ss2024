package com.softwaretesting.testing.util;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


class DebugFactorialTest {

    private DebugFactorial debugFactorial;

    @Test
    void checkFactorialForValueLessThan2(){
        int value = 1;
        int finalResult = debugFactorial.factorial(value);
        assertEquals(1, finalResult);
    }
    //
    @Test
    void checkFactorialForValueGreaterThan2(){
        int value = 5;
        int finalResult = debugFactorial.factorial(value);
        assertEquals(120, finalResult);
    }
}


