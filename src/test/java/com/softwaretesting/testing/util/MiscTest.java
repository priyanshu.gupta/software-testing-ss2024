package com.softwaretesting.testing.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class MiscTest {

    @Test
    void testSum() {
        assertEquals(5, Misc.sum(2, 3), "Sum of 2 and 3 should be 5");
        assertEquals(-1, Misc.sum(-2, 1), "Sum of -2 and 1 should be -1");
        assertEquals(0, Misc.sum(0, 0), "Sum of 0 and 0 should be 0");
    }

    @Test
    void testDivide() {
        assertEquals(2.0, Misc.divide(4, 2), "4 divided by 2 should be 2");
        assertEquals(5.0, Misc.divide(10, 2), "10 divided by 2 should be 5");
        assertThrows(RuntimeException.class, () -> Misc.divide(1, 0), "Division by zero should throw RuntimeException");
    }

    @Test
    void testIsColorSupported() {
        assertTrue(Misc.isColorSupported(Misc.Color.RED), "Red should be a supported color");
        assertTrue(Misc.isColorSupported(Misc.Color.YELLOW), "Yellow should be a supported color");
        assertTrue(Misc.isColorSupported(Misc.Color.BLUE), "Blue should be a supported color");
        assertThrows(IllegalArgumentException.class, () -> Misc.isColorSupported(null), "Null color should throw IllegalArgumentException");
    }

    @Test
    void testCalculateFactorial() {
        assertEquals(1, Misc.calculateFactorial(0), "Factorial of 0 should be 1");
        assertEquals(1, Misc.calculateFactorial(1), "Factorial of 1 should be 1");
        assertEquals(120, Misc.calculateFactorial(5), "Factorial of 5 should be 120");
        assertEquals(2, Misc.calculateFactorial(2), "Factorial of 2 should be 2");
    }

    @Test
    void testIsPrime() {
        assertTrue(Misc.isPrime(2, 2), "2 is a prime number");
        assertFalse(Misc.isPrime(4, 2), "4 is not a prime number");
        assertTrue(Misc.isPrime(17, 2), "17 is a prime number");
        assertFalse(Misc.isPrime(1, 2), "1 is not considered a prime number");

        // Additional edge cases
        assertFalse(Misc.isPrime(9, 2), "9 is not a prime number (3*3=9)");
        assertTrue(Misc.isPrime(23, 2), "23 is a prime number");
        assertFalse(Misc.isPrime(25, 2), "25 is not a prime number (5*5=25)");
        assertTrue(Misc.isPrime(29, 2), "29 is a prime number");

        // Edge cases around the i*i > n boundary
        assertTrue(Misc.isPrime(5, 2), "5 is a prime number");
        assertTrue(Misc.isPrime(7, 2), "7 is a prime number");
        assertFalse(Misc.isPrime(10, 2), "10 is not a prime number (2*5=10)");
        assertFalse(Misc.isPrime(12, 2), "12 is not a prime number (2*6=12)");

        // Specific tests for the condition i * i > n
        assertFalse(Misc.isPrime(49, 2), "49 is not a prime number (7*7=49)");
        assertTrue(Misc.isPrime(47, 2), "47 is a prime number");
        assertFalse(Misc.isPrime(48, 2), "48 is not a prime number (2*24=48)");
        assertFalse(Misc.isPrime(50, 2), "50 is not a prime number (2*25=50)");
        assertFalse(Misc.isPrime(51, 2), "51 is not a prime number (3*17=51)");
    }

    @Test
    void testIsEven() {
        assertTrue(Misc.isEven(2), "2 is even");
        assertFalse(Misc.isEven(3), "3 is not even");
        assertTrue(Misc.isEven(0), "0 is even");
        assertFalse(Misc.isEven(-1), "-1 is not even");
    }
}