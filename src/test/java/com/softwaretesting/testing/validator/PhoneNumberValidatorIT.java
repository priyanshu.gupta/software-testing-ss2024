package com.softwaretesting.testing.validator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PhoneNumberValidatorIT {
    private final PhoneNumberValidator phoneNumberValidator = new PhoneNumberValidator();

    @Test
    void testValidPhoneNumberWithCountryCode() {
        assertTrue(phoneNumberValidator.isValid("+491234567890"));
    }

    @Test
    void testValidPhoneNumberWithLeadingZero() {
        assertTrue(phoneNumberValidator.isValid("01234567890"));
    }

    @Test
    void testValidPhoneNumberWithCountryCodeAndSpaces() {
        assertFalse(phoneNumberValidator.isValid("+49 123 456 7890"));
    }

    @Test
    void testInvalidPhoneNumberWithLetters() {
        assertFalse(phoneNumberValidator.isValid("+49123456ABC"));
    }

    @Test
    void testInvalidPhoneNumberTooShort() {
        assertFalse(phoneNumberValidator.isValid("123"));
    }

    @Test
    void testInvalidPhoneNumberTooLong() {
        assertFalse(phoneNumberValidator.isValid("01234567890123456789"));
    }

    @Test
    void testInvalidPhoneNumberWithSpecialCharacters() {
        assertFalse(phoneNumberValidator.isValid("+49123456#$%^"));
    }

    @Test
    void testEmptyPhoneNumber() {
        assertFalse(phoneNumberValidator.isValid(""));
    }

    @Test
    void testNullPhoneNumber() {
        assertFalse(phoneNumberValidator.isValid(null));
    }

    @Test
    void testValidPhoneNumberWithDoubleDashes() {
        assertFalse(phoneNumberValidator.isValid("0123-456-7890"));
    }
}
